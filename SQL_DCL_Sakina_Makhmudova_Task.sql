CREATE USER rentaluser WITH PASSWORD 'rentalpassword';

REVOKE ALL ON DATABASE dvdrental FROM rentaluser;
GRANT CONNECT ON DATABASE dvdrental TO rentaluser;

GRANT SELECT ON TABLE customer TO rentaluser;

SELECT * FROM customer;

CREATE GROUP rental;
GRANT rental TO rentaluser;

GRANT INSERT, UPDATE ON TABLE rental TO rental;

INSERT INTO rental (rental_date, inventory_id, customer_id, return_date, staff_id)
VALUES (CURRENT_DATE, 1, 1, CURRENT_DATE, 1);

UPDATE rental SET return_date = CURRENT_DATE WHERE rental_id = 1;

CREATE OR REPLACE FUNCTION create_customer_role(first_name TEXT, last_name TEXT) RETURNS VOID AS $$
DECLARE
    role_name TEXT := 'client_' || first_name || '_' || last_name;
    customer_id INT;
BEGIN
    SELECT c.customer_id INTO customer_id 
    FROM customer c 
    WHERE c.first_name = first_name AND c.last_name = last_name;

    IF customer_id IS NOT NULL AND EXISTS (SELECT 1 FROM payment WHERE customer_id = customer_id) AND EXISTS (SELECT 1 FROM rental WHERE customer_id = customer_id) THEN
        EXECUTE 'CREATE ROLE ' || quote_ident(role_name);
        EXECUTE 'GRANT SELECT ON rental TO ' || quote_ident(role_name);
        EXECUTE 'GRANT SELECT ON payment TO ' || quote_ident(role_name);
    END IF;
END;
$$ LANGUAGE plpgsql;


SELECT create_customer_role('First', 'Last');

SET ROLE client_First_Last;

SELECT * FROM rental;

SELECT * FROM payment;



